<?php
include("../config.php");
$data = [];

$result = $db->rows("SELECT * FROM `finalhourstable` WHERE `userid` = '201700037' ORDER BY `shiftdate`");
foreach($result as $row) {
    $data[] = [
        'id'              => $row->userid,
        'title'           => $row->status,
        'start'           => $row->shiftdate,
        'end'             => $row->dates,
        'backgroundColor' => $row->'#6453e9',
        'textColor'       => $row->'#080808'
    ];
}

echo json_encode($data);
